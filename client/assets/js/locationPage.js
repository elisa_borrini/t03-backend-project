async function loadPage() {
    // URLSearchParams allows to easily retrieve query parameters from the url
    let id = new URLSearchParams(window.location.search).get('id');
    
    const response = await fetch(`/locations/${id}`)

    if(response.status == 200) {
        const data = await response.json();

        document.getElementById('location-name').innerText = data.name
        document.getElementById('location-city').innerText = data.city

        // Connection to all the dogs related to the chosen location
        let str = ""
        for(let dog of data.dogs) {
            str += `${dog.name} - ${dog.breed} --> <a href='/dogPage.html?id=${dog.id}'>LINK</a><br>`
        }
        document.getElementById('dogs').innerHTML = str
        
        document.title += " - " + data.name
    }
    else {
        alert("NOT FOUND")
        window.open('/', '_self')
    }
}

loadPage();