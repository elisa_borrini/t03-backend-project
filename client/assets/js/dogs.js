// Retrieving element from the HTML page.
let addBtn = document.getElementById('add-btn-basic');
let addBtnForm = document.getElementById('add-btn-form');

let container = document.getElementById("card-container");

// Function that create the template for our card.
// The name and breed can be omitted when called as they have default values
function createCard(name = "default-name", breed = "default-breed", id = -1) {
    let card = `<div class="card">
    <div class="image-container">
    <img class="dog-img" src="./assets/img/home-image.jpg" />
    </div>
    <span class="dog-name">${name}</span>
    <span class="dog-breed">${breed}</span>
    <button onclick=moveTo(${id})>Open description</button>
    </div>`

    return card;
}

// Function that adds a card with the default values
async function addCardString() {
    const response = await fetch('/dogs', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
                name: "default-name",
                breed: "default-breed",
                age: 10
            })
    })    

    if(response.status == 200) {
        // Calling function with default values. It's the same as createCard("default-name", "default-breed")
        container.innerHTML += createCard();
    }   
}

// Function that adds a card using the value in the form elements
async function addCardForm() {
    let nameInput = document.getElementById('dog-name');
    let breedInput = document.getElementById('dog-breed');

    let name = nameInput.value;
    let breed = breedInput.value;

    // Add a card only if both fields are filled
    if(name !== "" && breed !== "")
    {   
        const response = await fetch('/dogs', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                    name,
                    breed,
                    age: 10
                })
        })    

        if(response.status == 200) {
            const body = await response.json()
            container.innerHTML += createCard(name, breed, body.id);
            nameInput.value = "";
            breedInput.value = "";
        }
        else {
            alert("Something went wrong with your request.")
        }
    }
    else {
        alert("Please fill all the fields")
    }
}

// Registering the event to the buttons
addBtn.addEventListener('click', addCardString);
addBtnForm.addEventListener('click', addCardForm);

async function loadData() {
    const response = await fetch('/dogs')
    const body = await response.json()

    for(let dog of body) {
        container.innerHTML += createCard(dog.name, dog.breed, dog.id);
    }
}

function moveTo(id) {
    window.open('/dogPage.html?id=' + id, "_self")
}

loadData()