# T04 - Backend project
This is the repo for the webpage that was developed during the lecture on 23/03/2023

## Content

The project is the same as the one available in repository T00:

 - Four webpages (in the client folder):
	 - The homepage (index.html)
	 - The page containing the cards of the dogs (dogs.html)
	 - The page containing information about one dog (dogPage.html)
	 - The page containing the cards of the locations (locations.html)
	 - The page containing information about one location (locationPage.html)
	 - The about page (about.html)
	 - The contact page (contact.html)
 - CSS files (in folder assets/css):
	 - one for each webpage, using the same name
	 - one for the general CSS style shared between pages (general.css)
 - Images used in the webpages (in folder assets/img)

Additionally, a JS file has been added to manage the dynamic page for the dogs and locations.

## Server
The project is already set up with all the dependencies. Once downloaded/cloned, move in the folder using the terminal (or open the project using VSCode so that the terminal is already in the correct folder) and then call:

    npm install

This will ensure that all the dependencies are downloaded and available for the project.

When everything is ready, use:

    node index.js

This will start the server. The server listen on port 3000 and the following endpoint are available:

- GET
  - /dogs - Returns all the dogs 
  - /dogs/:id - Returns information about one dog specified by ID
  - /locations - Returns all the locations
  - /locations/:id - Returns information about one location specified by ID
  - /dogPageSSR/:id - SSR reproduction of dogPage.html
- POST
  - /dogs - Add a new dog to the DB